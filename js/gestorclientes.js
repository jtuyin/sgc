/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/

function getCliente(fila){
    var idCte=$(fila).children().first().text();
    $.ajax({
        method: "GET",
        url:'gestordeclientes/getoperacion/getCliente/'+idCte
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}

function getClienteContratante(cliente){
    $("#fnombres").val(cliente.innerHTML);
    $("#fid").val(cliente.value);
    $("#resultados").hide();
}

function agregarCliente(){//Formulario de captura de cliente
    $.ajax({
        method: "GET",
        url:'gestordeclientes/getoperacion/setCliente/0'
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}

function guardarCliente(objForm){
    var allinputs = objForm.children("input"); //sólo los inputs del formulario
    var idCte=objForm.find(allinputs[5]).val();
    var r = confirm("¿Guardar los datos del Cliente?");
    if (r === false) cancelarOperacion();
    else{
    $.ajax({
        method: "POST",
        url:'gestordeclientes/getoperacion/validarDatosCliente/-1',
        data:{
            idcliente:idCte,
            nombres:objForm.children("input[name=nombres]").val(),
            apellidos:objForm.children("input[name=apellidos]").val(),
            direccion:objForm.children("input[name=direccion]").val(),
            correo:objForm.children("input[name=correo]").val(),
            telefono:objForm.children("input[name=telefono]").val(),
            localidad:objForm.children("select[name=localidad]").val()
        }
    }).done(function(respuesta){
       $("aside").replaceWith(respuesta);
       actualizarSectionClientes();
       setTimeout(cancelarOperacionCliente,5000);
    });
    }
}

function actualizarSectionClientes(){
    $.ajax({
        method: "GET",
        url:'gestordeclientes/getoperacion/getAllClientes/0'
    }).done(function(respuesta){
        $("section table").replaceWith(respuesta); //Sustituir sólo la tabla       
    });
}

function buscarCliente(valor){
    if(valor!==""){
    $.ajax({
        method: "GET",
        url:'gestordeclientes/buscarClientes/'+valor
    }).done(function(respuesta){
        $("#resultados").html(respuesta);
        $("#resultados").show();
    });}else{
        $("#resultados").hide();
    }
}

function filtrarListaCliente(valor){
    if(valor!==""){
    $.ajax({
        method: "GET",
        url:'gestordeclientes/filtrarListaClientes/'+valor
    }).done(function(respuesta){
        $("section table").replaceWith(respuesta); //Sustituir sólo la tabla 
    });
    }else{
        actualizarSectionClientes();
    }
}

function cancelarOperacionCliente(){
    $.ajax({
        method: "GET",
        url:'gestordeclientes/getoperacion/aceptar/0'
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}