/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function getMensualidad(fila){
    var celdas=$(fila).children();
    var idMensualidad=celdas.first().text();
    $.ajax({
        method: "GET",
        url:'gestordecobranza/getoperacion/getMensualidad/'+idMensualidad
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
        /*if (celdas.last().text()==="Pagado"){
            //alert(celdas.last().text());
            $("#frmcobranza input[name=ffecha]").prop('disabled',true);
            $("aside .buttonNaranja").prop('disabled',true);//prop("disabled", false);
            $("aside .buttonNaranja").addClass('buttonGris');
            var importe = $("#frmcobranza input[name=importe]");
            importe.val(importe.val()+" (Pagado)");
        }*/
    });
}

function registrarPago(objForm){
   //var allinputs = objForm.children("input");
   var idfact=0;
   var idMes=0;
   idfact=objForm.children("input[name=idmensualidad]").val();
   var r = confirm("¿Confirma el registro de pago?");
   if (r === false) {
        cancelarOperacionCobranza();
   }else
   {idMes=objForm.children("input[name=idmes]").val()-1;
    $.ajax({
        method: "POST",
        url:'gestordecobranza/getoperacion/registrarPago/'+idfact,
        data:{
            idcliente:objForm.children("input[name=idcliente]").val(),
            idmes:objForm.children("input[name=idmes]").val(),
            fecha:objForm.children("input[name=ffecha]").val()
        }
    }).done(function(respuesta){
       $("aside").replaceWith(respuesta);
       actualizarPanelMain(idMes);
       setTimeout(cancelarOperacionCobranza,5000);
    });
   }
}

function actualizarPanelMain(mes){
    $.ajax({
        method: "GET",
        url:'gestordecobranza/getoperacion/getXMes/'+(mes+1)
    }).done(function(respuesta){
        var tabsPanel=$('section').children('.tabpanel');
        var numTabs=tabsPanel.length;
        for (var i=0;i<numTabs;i++){
            if (jQuery.css(tabsPanel[i],"display")!=="none"){ 
                $(tabsPanel[i]).html(respuesta);//Sustituir sólo la tabla mostrada
            }
        }
    });
}

function cambiarPaginaTabs(mes,obj){
    var mes=1;
    if($(obj).text()==="<"){
        mes=-1;
    }
    $.ajax({
        method: "GET",
        url:'gestordecobranza/getcontenidomain/getPagina/'+mes,
        data:{
            anio:"2017"
        }
    }).done(function(respuesta){
        $("section").replaceWith(respuesta);
    });
}

function cancelarOperacionCobranza(){
    $.ajax({
        method: "GET",
        url:'gestordecobranza/getoperacion/aceptar/-1'
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}
/******************************************************+++
 * Cálculo de las mensualidades
 */

function calcularMensualidad(){
    $.ajax({
        method: "GET",
        url:'gestordecobranza/getoperacion/getCalculoMensualidades/'+$("select[name=mesSeleccionado]").val()
    }).done(function(respuesta){
        $("#generadorMensualidad").html(respuesta);
    });
}

function confirmarCalculo(){
     $.ajax({
        method: "GET",
        url:'gestordecobranza/getoperacion/generarMensualidad/0'
    }).done(function(respuesta){
        $("#generadorMensualidad").replaceWith(respuesta);
        $("#generadorMensualidad").show();
    });
}
function cancelarCalculo(){
    $("#generadorMensualidad").hide();
    $.ajax({
        method: "GET",
        url:'gestordecobranza/cancelarcalculomensualidad'
    }).done(function(respuesta){
  $("#generadorMensualidad").replaceWith(respuesta);
    });
}

function finalizarFacturacion(){
    //$("#generadorMensualidad").hide();
    $.ajax({
        method: "GET",
        url:'gestordecobranza/cancelarcalculomensualidad'
    }).done(function(respuesta){
  $("#generadorMensualidad").replaceWith(respuesta);
    });
}
function imprimirFacturacion(mes){
    mes=$(".tabactiva").text();
    window.open('gestordecobranza/getContenidoMain/getImpresion/'+mes,'_blank');
    return false;
}


/*


function registrarPago(){
    $.ajax({
        method: "GET",
        url:'gestordecobaranza/registrarpago'
    }).done(function(respuesta){
        $("aside").html(respuesta);
    });
}


*/

