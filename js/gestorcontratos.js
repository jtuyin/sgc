/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getContrato(fila){
    celdas=$(fila).children();
    var idCto=celdas.first().text();
     $.ajax({
        method: "GET",
        url:'gestordecontratos/getcontrato/'+idCto
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}

function agregarContrato(){
    $.ajax({
        method: "GET",
        url:'gestordecontratos/agregarcontrato'
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}

function guardarContrato(objForm){
   var allinputs = objForm.children("input");
   var idCte=0;
   var tabServicio;
   if(allinputs.length>5){
       idCte=objForm.find(allinputs[4]).val();
       var r = confirm("Actualizar los datos del Contrato?"+idCte);
        if (r === false) {
            cancelarOperacionContrato();
        }else{
            tabServicio=(objForm.children("select[name=servicio]").val()-1);
    $.ajax({
        method: "POST",
        url:'gestordecontratos/validarDatosContrato',
        data:{
            idcliente:idCte,
            nombres:objForm.children("input[name=nombres]").val(),
            idcontrato:objForm.children("input[name=idcontrato]").val(),
            servicio:objForm.children("select[name=servicio]").val(),
            inicio:objForm.children("input[name=inicio]").val(),
            corte:objForm.children("input[name=corte]").val(),
            limite:objForm.children("input[name=limite]").val()
        }
    }).done(function(respuesta){
       $("aside").html(respuesta);
       actualizarSectionContratos(tabServicio);
    });
        }
   }
    
}

function actualizarSectionContratos(tab){
    $.ajax({
        method: "GET",
        url:'gestordecontratos/getcontratos',
        data:{
            tabAct:tab
        }
    }).done(function(respuesta){
        $("section").html(respuesta);
    });
}

function cancelarOperacionContrato(){
    $.ajax({
        method: "GET",
        url:'gestordecontratos/cancelarOperacion',
        data:{
            operacion:"no"
        }
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}

