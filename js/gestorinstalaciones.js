/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/

function getInstalacion(fila){
    var idInst=$(fila).children().first().text();
    $.ajax({
        method: "GET",
        url:'gestordeinstalaciones/getoperacion/getInstalacion/'+idInst
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}
/*
function getInstalacionContratante(cliente){
    $("#fnombres").val(cliente.innerHTML);
    $("#fid").val(cliente.value);
    $("#resultados").hide();
}*/

function agregarInstalacion(){//Formulario de captura de cliente
    $.ajax({
        method: "GET",
        url:'gestordeinstalaciones/getoperacion/setInstalacion/0'
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}

function guardarInstalacion(objForm){
    var allinputs = objForm.children("input"); //sólo los inputs del formulario
    var idInst=objForm.find(allinputs[5]).val();
    var r = confirm("¿Guardar los datos del Instalacion?");
    if (r === false) cancelarOperacion();
    else{
    $.ajax({
        method: "POST",
        url:'gestordeinstalaciones/getoperacion/validarDatosInstalacion/-1',
        data:{
            idcliente:idInst,
            equipo:objForm.children("input[name=equipo]").val(),
            ip:objForm.children("input[name=ip]").val(),
            ubicacion:objForm.children("input[name=ubicacion]").val(),
            observacion:objForm.children("input[name=observacion]").val(),
            idcontrato:objForm.children("input[name=idcontrato]").val()
            //localidad:objForm.children("select[name=localidad]").val()
        }
    }).done(function(respuesta){
       $("aside").replaceWith(respuesta);
       actualizarSectionInstalaciones();
       setTimeout(cancelarOperacionInstalacion,5000);
    });
    }
}

function actualizarSectionInstalaciones(){
    $.ajax({
        method: "GET",
        url:'gestordeinstalaciones/getoperacion/getAllInstalaciones/0'
    }).done(function(respuesta){
        $("section table").replaceWith(respuesta); //Sustituir sólo la tabla       
    });
}

function buscarInstalacion(valor){
    if(valor!==""){
    $.ajax({
        method: "GET",
        url:'gestordeinstalaciones/buscarInstalaciones/'+valor
    }).done(function(respuesta){
        $("#resultados").html(respuesta);
        $("#resultados").show();
    });}else{
        $("#resultados").hide();
    }
}

function filtrarListaInstalacion(valor){
    if(valor!==""){
    $.ajax({
        method: "GET",
        url:'gestordeinstalaciones/filtrarListaInstalaciones/'+valor
    }).done(function(respuesta){
        $("section table").replaceWith(respuesta); //Sustituir sólo la tabla 
    });
    }else{
        actualizarSectionInstalaciones();
    }
}

function cancelarOperacionInstalacion(){
    $.ajax({
        method: "GET",
        url:'gestordeinstalaciones/getoperacion/aceptar/0'
    }).done(function(respuesta){
        $("aside").replaceWith(respuesta);
    });
}