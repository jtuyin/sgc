<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class MServicio extends CI_Model{
 
 function getServicios(){
   $this -> db -> select('*');
   $this -> db -> from('concepto');
   $this -> db -> where('tipo', '1');
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function getServicioId($idCte){
   $this -> db -> select('*');
   $this -> db -> from('cliente');
   $this -> db -> where('idcliente', $idCte);
   //$this -> db -> where('clave',$clave);//MD5($clave));
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 //Modificar para agregar Servicio
function agregarCliente($datosCliente){
   $this -> db -> insert('cliente',$datosCliente);
   $this->db->select('MAX(idcliente)');
   $this -> db -> from('cliente');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 //Para Actualizar servicio
 function actualizarCliente($datosCliente,$idCte){
    $this -> db -> where('idcliente', $idCte);
    $this -> db -> update('cliente',$datosCliente);
   $this->db->select('MAX(idcliente)');
   $this -> db -> from('cliente');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}