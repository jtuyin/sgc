<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class MCliente extends CI_Model{
 
 function getClientes(){
   $this -> db -> select('idcliente,nombres,apellidos,correo,telefono');
   $this -> db -> from('cliente');
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 /*
 function getIdsClientes(){
   $this -> db -> select('idcliente,concat(nombres," ",apellidos) as Cliente, mensualidad.importe_total, estado');
   $this -> db -> from('cliente');
   $this->db->join('mensualidad','mensualidad.cliente_idcliente=cliente.idcliente');
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }*/
 
 function getClienteId($idCte){
   $this -> db -> select('*');
   $this -> db -> from('cliente');
   $this -> db -> where('idcliente', $idCte);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function getClientesBuscados($filtro){
   $this -> db -> select('idcliente,nombres,apellidos,,correo,telefono');
   $this -> db -> from('cliente');
   $this -> db -> like('nombres', $filtro);
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
function agregarCliente($datosCliente){
   $this -> db -> insert('cliente',$datosCliente);
   $this->db->select('MAX(idcliente)');
   $this -> db -> from('cliente');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function actualizarCliente($datosCliente,$idCte){
    $this -> db -> where('idcliente', $idCte);
    $this -> db -> update('cliente',$datosCliente);
   $this->db->select('MAX(idcliente)');
   $this -> db -> from('cliente');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}