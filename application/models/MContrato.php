<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class MContrato extends CI_Model{
 
 function getContratos(){
   $this -> db -> select('*');
   $this -> db -> from('contrato');
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function getContratosXServicio($idServicio="1"){
    $this->db->select('contrato.idcontrato, concat(cliente.nombres," ",cliente.apellidos) as Nombre, contrato.fechainicio, contrato.diacorte, contrato.diaslimite');
    $this->db->from('contrato');
    $this->db->join('cliente','cliente.idcliente = contrato.cliente_idcliente');
    $this->db->where('contrato.concepto_idconcepto = '.$idServicio);
    $query=$this -> db ->get();
    if($query -> num_rows()>=1)
    {
        return $query;
    }
    else
    {
        return false;
    }
 }
 
 function getContratoId($idCnto){
   $this -> db -> select('contrato.idcontrato, contrato.cliente_idcliente, concat(cliente.nombres," ",cliente.apellidos) as Nombre,contrato.concepto_idconcepto as servicio_idservicio, contrato.fechainicio, contrato.diacorte, contrato.diaslimite');
   $this -> db -> from('contrato');
   $this->db->join('cliente','cliente.idcliente = contrato.cliente_idcliente');
   $this -> db -> where('idcontrato', $idCnto);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

function agregarContrato($datosContrato){
   $this -> db -> insert('contrato',$datosContrato);   
   $this->db->select('MAX(idcontrato)');
   $this -> db -> from('contrato');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function actualizarContrato($datosContrato,$idCto){
    $this -> db -> where('idcontrato', $idCto);
    $this -> db -> update('contrato',$datosContrato);
   $this->db->select('MAX(idcontrato)');
   $this -> db -> from('contrato');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}