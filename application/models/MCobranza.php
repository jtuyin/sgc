<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class MCobranza extends CI_Model {

    function getMensualidad($id_factura) {
        $query = $this->db->query(
                "SELECT idfactura,importe_total,mes,anio,estado,idcliente,nombres,apellidos,correo"
                . " FROM gcs.mensualidad, cliente"
                . " WHERe cliente_idcliente=idcliente AND idfactura=" . $id_factura
        );
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getFacturasMes($mes) {
        $this->db->select('mensualidad.idfactura,cliente.nombres,cliente.apellidos,'
                . 'mensualidad.importe_total,mensualidad.estado,'
                . 'contrato.diacorte,contrato.diaslimite');
        $this->db->from('mensualidad');
        $this->db->join('cliente', 'mensualidad.cliente_idcliente=cliente.idcliente');
        $this->db->join('contrato', 'mensualidad.cliente_idcliente=contrato.cliente_idcliente ');
        $this->db->where('mensualidad.mes ', $mes);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
  
    function getCalculoMensualidades() {
        $query = $this->db->query("SELECT idcliente,nombres, apellidos, descripcion, monto, diacorte,diaslimite FROM  gcs.contrato inner join gcs.cliente on gcs.cliente.idcliente = gcs.contrato.cliente_idcliente inner join gcs.concepto on gcs.contrato.concepto_idconcepto = gcs.concepto.idconcepto, gcs.importe where gcs.contrato.concepto_idconcepto=gcs.importe.concepto_idconcepto");
        if ($query->num_rows() >= 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getCalculoCortoMensualidades() {
        $query = $this->db->query("SELECT monto,cliente_idcliente as cliente FROM  gcs.contrato inner join gcs.concepto on gcs.contrato.concepto_idconcepto = gcs.concepto.idconcepto, gcs.importe where gcs.contrato.concepto_idconcepto=gcs.importe.concepto_idconcepto");
        if ($query->num_rows() >= 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function setFacturacionMensualidades($datosFacturas) {
        $this->db->insert('mensualidad', $datosFacturas);
        $this->db->select('MAX(idfactura)');
        $this->db->from('mensualidad');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function setRegistroPago($datosPago) {// datosPago es un arreglo con el id de la factura
        $this->db->insert('pago',$datosPago);
        $this->db->select('COUNT(mensualidad_idfactura)');
        $this->db->from('pago');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}



  /*
 function getMensualidadXCorte($diaCrt="7"){ // Mensualidades pagadas para webservice de app móvil
    $this->db->select('mensualidad.idfactura,mensualidad.importe_total,mensualidad.mes,mensualidad.anio');
    $this->db->from('mensualidad');
    $this->db->join('cliente','cliente.idcliente = mensualidad.cliente_idcliente');
    //$this->db->where('mensualidad.mes = '.$diaCrt);
    $query=$this -> db ->get();
    if($query -> num_rows()>=1)
    {
        return $query;
    }
    else
    {
        return false;
    } 
 }*/