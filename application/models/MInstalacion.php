<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class MInstalacion extends CI_Model{
 
 function getInstalaciones(){
   $this -> db -> select('*');//'idinstalacion,equipo,apellidos,correo,telefono');
   $this -> db -> from('instalacion');
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function getInstalacionId($idInst){
   $this -> db -> select('*');
   $this -> db -> from('instalacion');
   $this -> db -> where('idinstalacion', $idInst);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function getInstalacionesBuscados($filtro){
   $this -> db -> select('idinstalacion,nombres,apellidos,,correo,telefono');
   $this -> db -> from('instalacion');
   $this -> db -> like('nombres', $filtro);
   $query = $this -> db -> get();
   if($query -> num_rows()>=1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
function agregarInstalacion($datosInstalacion){
   $this -> db -> insert('instalacion',$datosInstalacion);
   $this->db->select('MAX(idinstalacion)');
   $this -> db -> from('instalacion');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function actualizarInstalacion($datosInstalacion,$idInst){
    $this -> db -> where('idinstalacion', $idInst);
    $this -> db -> update('instalacion',$datosInstalacion);
   $this->db->select('MAX(idinstalacion)');
   $this -> db -> from('instalacion');
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows()==1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}