<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * Operaciones CRUD para los registros de clientes
 */
class GestorDeClientes extends CI_Controller {
    //Template para la tabla que enlista los clientes
    var $tmpl = array ( 
            'table_open'  => '<table class="scroll" >',
            'heading_row_start'   => '<tr class="basico">',
            'heading_row_end'     => '</tr>',
            'row_start'           => '<tr onclick="getCliente(this)">',
            'row_end'             => '</tr>',
            'row_alt_start'       => '<tr onclick="getCliente(this)">',
            'row_alt_end'         => '</tr>'
        );
    
 function index(){
   if($this->session->userdata('autenticado'))
   {
       $this->cargarPagina();
   }
   else{     //Si no hay sesion redirigir al login
    redirect('/Acceso/', 'location', 301);
   }
 }
 
 private function cargarPagina(){
     $session_data['usuario'] = $this->session->userdata('usuario');
     $data['usuario'] = $session_data['usuario'];
     $this->load->view('vheader', $data);
     
     $this->getNavigation();
     $this->getPanelMain();    
     $this->getPanelOperaciones();
     
     $this->load->view('vfooter'); 
 }

 private function getNavigation(){
     $active=array("act"=>2);
     $this->load->library("menu",$active);
     $this->menu->setActivo(2);
     $data["nav"]=$this->menu->getMenu();
     $this->load->view('vnav',$data); 
 }

 private function getPanelMain(){
     $data["tabla"]= $this->getListaClientes();
     $this->load->view('vsectionclientes',$data);
 }
 
 private function getPanelOperaciones() {
     $data["operacion"] = $this->session->userdata('operCte');
     switch ($data["operacion"]) {
        case "getCliente":   
        case "setCliente":
            $id=$this->session->userdata('idcte');
            $data["cte"] = $this->getCliente($id);
        break; 
        case "validarDatosCliente":
            $data["mensaje"]='<div class="error">No est&aacute; completa la informaci&oacute;n'
                . ' Los datos no se guardaron</div>';
        break;
        case "guardarCliente":
            $data["mensaje"]='<div class="basico-borde">Cliente Guardado Exitosamente!!!</div>'
                .'<div class="basico basico-borde">De clic en otro cliente </div>';
        break;
        default :
            $data["mensaje"]='<div class="basico basico-borde">De clic en un cliente para ver informaci&oacute;n </div>';
     }
     $this->load->view('vasideclientes',$data);
 }
 
 private function getListaClientes(){
    $this->load->model('MCliente');     
    $this->load->library('table');
    
    $clientes=  $this->MCliente->getClientes();
     $this->table->set_template($this->tmpl);
     $this->table->set_heading(array('ID','Nombre','Apellido','Telefono','Correo'));
     if($clientes){
         foreach($clientes as $clte){
            $row_data=array($clte->idcliente,$clte->nombres,$clte->apellidos,$clte->telefono,$clte->correo);
            $this->table->add_row($row_data);
         }
         $listadoClientes= $this->table->generate();
     }else{
         $listadoClientes="No hay Clientes";
     }
     return $listadoClientes;
 }
 
 function filtrarListaClientes($datoFiltro){
    $this->load->model('MCliente');     
    $this->load->library('table');
    $clientes=  $this->MCliente->getClientesBuscados($datoFiltro);
     $this->table->set_template($this->tmpl);
     $this->table->set_heading(array('ID','Nombre','Apellido','Telefono','Correo'));
     if($clientes){
         foreach($clientes as $clte){
            $row_data=array($clte->idcliente,$clte->nombres,$clte->apellidos,$clte->telefono,$clte->correo);
            $this->table->add_row($row_data);
         }
         $listadoClientes= $this->table->generate();
     }else{
         $listadoClientes="No hay Clientes";
     }
     echo $listadoClientes;
 }
 
 function buscarClientes($datoFiltro) {
        $this->load->model('MCliente');
        $clientes = $this->MCliente->getClientesBuscados($datoFiltro);
        $listadoClientes = '<select name="clientes" size="5">';
        if ($clientes) {
            foreach ($clientes as $row) {
                $listadoClientes = $listadoClientes . '<option value="' . $row->idcliente . '" onclick="getClienteContratante(this)">' . $row->nombres . ' ' . $row->apellidos . '</option>';
            }
        } else {
            $listadoClientes = $listadoClientes . '<option value="0">No hay clientes</option>';
        }
        $listadoClientes=$listadoClientes.'</select>';
        echo $listadoClientes;
    }

 private function getCliente($id) {
        $this->load->model('MCliente');
        $cliente = $this->MCliente->getClienteId($id);
        if ($cliente) {
            return $cliente[0];
        }
    }
      
 private function validarDatosCliente(){
        $this->load->library('form_validation'); //This method will have the credentials validation
        $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required');
        $this->form_validation->set_rules('direccion', 'Direccion', 'trim');
        $this->form_validation->set_rules('correo', 'Correo', 'trim');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('localidad','Localidad','trim|required'); 
        if($this->form_validation->run() == FALSE){//validacion fallida. Datos no guardados
            $this->getPanelOperaciones();
        }else{ //Validación de campos exitosa.             
            $this->guardarCliente();
        }
    }
    
 private function guardarCliente(){
        $this->session->set_userdata('operCte','guardarCliente');
        $this->load->model('MCliente', '', TRUE);
        $idcliente =$this->input->post('idcliente');
        $cliente["nombres"] = $this->input->post('nombres');
        $cliente["apellidos"]=$this->input->post('apellidos');
        $cliente["direccion"] = $this->input->post('direccion');
        $cliente["correo"] = $this->input->post('correo');
        $cliente["telefono"] = $this->input->post('telefono');
        $cliente["localidad_idlocalidad"] = $this->input->post('localidad');
        if($idcliente!=0){
            $result = $this->MCliente->actualizarCliente($cliente,$idcliente);//query the database            
        }else{
            $result = $this->MCliente->agregarCliente($cliente);//query the database           
        }
        if($result){            
            $this->getPanelOperaciones();         
        }else{
            $this->form_validation->set_message('guardarCliente', 'Los datos no se insertaron');
            return false;
        }
    }
 
 function getOperacion($operacionCliente,$dato){
      $this->session->set_userdata('operCte',$operacionCliente);
      switch($operacionCliente){
          case "aceptar"://Cancela la operación
          case "getCliente":
          case "setCliente":
              $this->session->set_userdata('idcte',$dato);
              $this->getPanelOperaciones();
          break;
          case "getAllClientes":
              echo $this->getListaClientes();
              
          break;
          case "validarDatosCliente":
              $this->validarDatosCliente();
          break;
          
      }
 }
}
