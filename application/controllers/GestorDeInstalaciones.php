<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * Operaciones CRUD para los registros de instalaciones
 */
class GestorDeInstalaciones extends CI_Controller {
    //Template para la tabla que enlista los instalaciones
    var $tmpl = array ( 
            'table_open'  => '<table class="scroll" >',
            'heading_row_start'   => '<tr class="basico">',
            'heading_row_end'     => '</tr>',
            'row_start'           => '<tr onclick="getInstalacion(this)">',
            'row_end'             => '</tr>',
            'row_alt_start'       => '<tr onclick="getInstalacion(this)">',
            'row_alt_end'         => '</tr>'
        );
    
 function index(){
   if($this->session->userdata('autenticado'))
   {
       $this->cargarPagina();
   }
   else{     //Si no hay sesion redirigir al login
     redirect('acceso', 'refresh');
   }
 }
 
 private function cargarPagina(){
     $session_data['usuario'] = $this->session->userdata('usuario');
     $data['usuario'] = $session_data['usuario'];
     $this->load->view('vheader', $data);
     
     $this->getNavigation();
     $this->getPanelMain();    
     $this->getPanelOperaciones();
     
     $this->load->view('vfooter'); 
 }

 private function getNavigation(){
     $active=array("act"=>2);
     $this->load->library("menu",$active);
     $this->menu->setActivo(5);
     $data["nav"]=$this->menu->getMenu();
     $this->load->view('vnav',$data); 
 }

 private function getPanelMain(){
     $data["tabla"]= $this->getListaInstalaciones();
     $this->load->view('vsectioninstalaciones',$data);
 }
 
 private function getPanelOperaciones() {
     $data["operacion"] = $this->session->userdata('operInst');
     switch ($data["operacion"]) {
        case "getInstalacion":   
        case "setInstalacion":
            $id=$this->session->userdata('idInst');
            $data["inst"] = $this->getInstalacion($id);
        break; 
        case "validarDatosInstalacion":
            $data["mensaje"]='<div class="error">No est&aacute; completa la informaci&oacute;n'
                . ' Los datos no se guardaron</div>';
        break;
        case "guardarInstalacion":
            $data["mensaje"]='<div class="basico-borde">Instalaci&ocute; Guardado Exitosamente!!!</div>'
                .'<div class="basico basico-borde">De clic en otra instalación </div>';
        break;
        default :
            $data["mensaje"]='<div class="basico basico-borde">De clic en una instalaci&oacute;n para ver informaci&oacute;n </div>';
     }
     $this->load->view('vasideinstalaciones',$data);
 }
 
 private function getListaInstalaciones(){
    $this->load->model('minstalacion');     
    $this->load->library('table');
    
    $instalaciones=  $this->minstalacion->getInstalaciones();
     $this->table->set_template($this->tmpl);
     $this->table->set_heading(array('ID','Equipo','Direccion','Ubicacion','Observacion'));
     if($instalaciones){
         foreach($instalaciones as $clte){
            $row_data=array($clte->idinstalacion,$clte->equipo,$clte->ip,$clte->ubicacion,$clte->observacion);
            $this->table->add_row($row_data);
         }
         $listadoInstalaciones= $this->table->generate();
     }else{
         $listadoInstalaciones="No hay Instalaciones";
     }
     return $listadoInstalaciones;
 }
 
 function filtrarListaInstalaciones($datoFiltro){
    $this->load->model('minstalacion');     
    $this->load->library('table');
    $instalaciones=  $this->minstalacion->getInstalacionesBuscadas($datoFiltro);
     $this->table->set_template($this->tmpl);
     $this->table->set_heading(array('ID','Nombre','Apellido','Telefono','Correo'));
     if($instalaciones){
         foreach($instalaciones as $clte){
            $row_data=array($clte->idinstalacion,$clte->nombres,$clte->apellidos,$clte->telefono,$clte->correo);
            $this->table->add_row($row_data);
         }
         $listadoInstalaciones= $this->table->generate();
     }else{
         $listadoInstalaciones="No hay Instalaciones";
     }
     echo $listadoInstalaciones;
 }
 
 function buscarInstalaciones($datoFiltro) {
        $this->load->model('minstalacion');
        $instalaciones = $this->minstalacion->getInstalacionesBuscados($datoFiltro);
        $listadoInstalaciones = '<select name="instalaciones" size="5">';
        if ($instalaciones) {
            foreach ($instalaciones as $row) {
                $listadoInstalaciones = $listadoInstalaciones . '<option value="' . $row->idinstalacion . '" onclick="getInstalacionContratante(this)">' . $row->nombres . ' ' . $row->apellidos . '</option>';
            }
        } else {
            $listadoInstalaciones = $listadoInstalaciones . '<option value="0">No hay instalaciones</option>';
        }
        $listadoInstalaciones=$listadoInstalaciones.'</select>';
        echo $listadoInstalaciones;
    }

 private function getInstalacion($id) {
        $this->load->model('minstalacion');
        $instalacion = $this->minstalacion->getInstalacionId($id);
        if ($instalacion) {
            return $instalacion[0];
        }
    }
      
 private function validarDatosInstalacion(){
        $this->load->library('form_validation'); //This method will have the credentials validation
        $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required');
        $this->form_validation->set_rules('direccion', 'Direccion', 'trim');
        $this->form_validation->set_rules('correo', 'Correo', 'trim');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('localidad','Localidad','trim|required'); 
        if($this->form_validation->run() == FALSE){//validacion fallida. Datos no guardados
            $this->getPanelOperaciones();
        }else{ //Validación de campos exitosa.             
            $this->guardarInstalacion();
        }
    }
    
 private function guardarInstalacion(){
        $this->session->set_userdata('operInst','guardarInstalacion');
        $this->load->model('minstalacion', '', TRUE);
        $idinstalacion =$this->input->post('idinstalacion');
        $instalacion["nombres"] = $this->input->post('nombres');
        $instalacion["apellidos"]=$this->input->post('apellidos');
        $instalacion["direccion"] = $this->input->post('direccion');
        $instalacion["correo"] = $this->input->post('correo');
        $instalacion["telefono"] = $this->input->post('telefono');
        $instalacion["localidad_idlocalidad"] = $this->input->post('localidad');
        if($idinstalacion!=0){
            $result = $this->minstalacion->actualizarInstalacion($instalacion,$idinstalacion);//query the database            
        }else{
            $result = $this->minstalacion->agregarInstalacion($instalacion);//query the database           
        }
        if($result){            
            $this->getPanelOperaciones();         
        }else{
            $this->form_validation->set_message('guardarInstalacion', 'Los datos no se insertaron');
            return false;
        }
    }
 
 function getOperacion($operacionInstalacion,$dato){
      $this->session->set_userdata('operInst',$operacionInstalacion);
      switch($operacionInstalacion){
          case "aceptar"://Cancela la operación
          case "getInstalacion":
          case "setInstalacion":
              $this->session->set_userdata('idInst',$dato);
              $this->getPanelOperaciones();
          break;
          case "getAllInstalaciones":
              echo $this->getListaInstalaciones();
              
          break;
          case "validarDatosInstalacion":
              $this->validarDatosInstalacion();
          break;
          
      }
 }
}
