<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MapaInstalaciones extends CI_Controller {
    public function index(){
        // Load the library
		$this->load->library('googlemaps');
// Initialize our map. Here you can also pass in additional parameters for customising the map (see below)

		// Initialize the map, passing through any parameters
 $config['center'] = "21.161, -89.655";
 $config['zoom'] = "17";

		$this->googlemaps->initialize($config);
// Create the map. This will return the Javascript to be included in our pages <head></head> section and the HTML code to be
// placed where we want the map to appear.
		// Set the marker parameters as an empty array. Especially important if we are using multiple markers

$marker = array();
// Specify an address or lat/long for where the marker should appear.
$marker[' position '] = '21.161, -89.655';
// Once all the marker parameters have been specified lets add the marker to our map
$this->googlemaps->add_marker($marker);

		$data['map'] = $this->googlemaps->create_map();
// Load our view, passing the map data that has just been created
$this->load->view('instalaciones/vmapsinstalaciones', $data);

		
    }
}
