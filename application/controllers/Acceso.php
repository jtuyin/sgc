<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acceso extends CI_Controller {
    public function index(){
        $this->autenticarUsuario();
    }
    private function autenticarUsuario(){
        
        $this->load->view('vacceso');
    }
    public function validarUsuario(){
        
        $this->load->model('MUsuario');        
        $this->form_validation->set_rules('usuario','Usuario del Sistema','trim|required');
        $this->form_validation->set_rules('clave','Clave de acceso','trim|required|callback_check_database');
        $this->form_validation->set_message('required', '** <strong>%s</strong> es requerido');
        if($this->form_validation->run() == FALSE){//Field validation failed.  User redirected to login page
           redirect('acceso');
        }else{ //Go to private area 
            redirect('inicio');
        }
    }
    function check_database($clave){
        //Field validation succeeded.  Validate against database
        $usuario = $this->input->post('usuario');  
        $result = $this->MUsuario->ingresar($usuario, $clave);//query the database
        if($result){ // Establecer valores de operaciones al iniciar la sesión
            $sess_array = array();
            foreach($result as $row){
                $sess_array = array(
                    'idusuario' => $row->id,
                    'usuario' => $row->nombre,
                    'autenticado'=>true,
                    'operacion'=>'no'
                );
                $this->session->set_userdata($sess_array);
            }
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
