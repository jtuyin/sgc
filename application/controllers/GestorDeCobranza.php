<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * Operaciones para los registros de cobranza
 */
class GestorDeCobranza extends CI_Controller {
        
    var $tmpl = array(
                'table_open' => '<table class="scroll">',
                'heading_row_start' => '<tr class="basico">',
                'heading_row_end' => '</tr>',
                'row_start' => '<tr onclick="getMensualidad(this)" >',
                'row_end' => '</tr>',
                'row_alt_start' => '<tr onclick="getMensualidad(this)" >',
                'row_alt_end' => '</tr>'
            );
    var $tmpl2 = array(
                'table_open' => '<table class="scroll">',
                'heading_row_start' => '<tr class="basico">',
                'heading_row_end' => '</tr>'
            );
    
    function index() {
        if ($this->session->userdata('autenticado')) {
            $this->cargarPagina();
        } else {     //Si no hay sesion redirigir al login
            redirect('acceso', 'refresh');
        }
    }
//*******************************************************************
//  Generación del contenido de la layout     
//*******************************************************************
    private function cargarPagina() {
        //$session_data['usuario'] = 
        $data['usuario'] = $this->session->userdata('usuario');//$session_data['usuario'];
        $this->load->view('vheader', $data);
        //===========================================================
        $this->getNavigation();
        $this->getPanelMain();
        $this->getPanelOperaciones();
        //===========================================================
        $this->load->view('vfooter');
    }
//Contenido para la sección de Navegación de funciones
    private function getNavigation() {
        $active = array("act" => 4);
        $this->load->library("menu", $active);
        $this->menu->setActivo(4);
        $data["nav"] = $this->menu->getMenu();        
        $this->load->view('vnav', $data);
    }
//Contenido principal de los datos
    private function getPanelMain() {
        //verificar el mes inicio para obtener cuatro elementos del arreglo
        if($this->session->userdata('mesIni')==null || $this->session->userdata('mesIni')<=2){
            $mesInicio =$this->session->set_userdata('mesIni',date('n'));
        }
            $mesInicio =$this->session->userdata('mesIni');
        $this->load->library("calendario");
        $this->calendario->setMesActivo($mesInicio);
        $meses = $this->calendario->getMeses();
        $this->load->library('tab');     
        foreach ($meses as $num_mes => $nom_mes) {
            $this->tab->addTab($nom_mes, $this->getMensualidadesXMes($num_mes));
        }
        $this->tab->addSelector("X");
        $this->tab->addPaginador($mesInicio);
        
        $data["contenidoMain"] = $this->tab->generar();
        $data["tabAct"] =2;//((int)$mesInicio["ini"])%12;
        $this->load->view('vsectioncobranza', $data);  
    }
//Contenido de los detalles y operaciones
    private function getPanelOperaciones() {
        $data['operacion'] = $this->session->userdata('operCbza'); //Verificar operacion para el panel aside        
        $id = $this->session->userdata('idMens');
        switch($data['operacion']){
            case "getMensualidad":                
                $data["mensualidad"]=$this->getMensualidad($id);
            break;
            case "registrarPago":
                $data["pago"]=$this->setRegistroPago($id);
                $data["mensaje"]='<div class="basico-borde">Pago registrado Exitosamente!!!</div>';                
            break;
            default :
                $data["mensaje"]='<div class="basico basico-borde">De clic en una mensualidad para ver detalles </div>';
        }
        $this->load->view('vasidecobranza',$data);
    }

//-----------------------------------------------------------------
//Getters
//-----------------------------------------------------------------
    private function getMensualidad($id) {
        $this->load->library("calendario");
        $this->load->model('mcobranza');
        $mensualidad = $this->mcobranza->getMensualidad($id);
        if ($mensualidad){
            $this->session->set_userdata('idFactura', $id);
            $mensualidad[0]->mesNombre =$this->calendario->getMes($mensualidad[0]->mes);            
            return $mensualidad[0];
        }
    }
    
    private function getCalculoMensualidad($mes){
        $this->load->model('mcobranza');
         $this->session->set_userdata('mesfacturado',$mes);
        $mensualidades =  $this->mcobranza->getCalculoMensualidades();
        if($mensualidades){
            $this->load->library('table');
            $this->table->set_template($this->tmpl2);
            $this->table->set_heading("id","Nombre","Apellido","Importe","Fecha");
            foreach($mensualidades as $mensual){
                $mensualidad= (object)$mensual;
                $row_data = array($mensualidad->idcliente,'<input type="checkbox" value="'.$mensualidad->idcliente.'" checked> '.$mensualidad->nombres,$mensualidad->apellidos,"$ ".$mensualidad->monto,$mensualidad->diacorte); 
               $this->table->add_row($row_data);
            }            
            echo $this->table->generate();
            echo '<button class="button buttonNaranja" onclick="confirmarCalculo(\'generadorMensualidad\')">Aceptar</button>'
            .'<button id="generadorMensualidadBtnCancelar" class="button" onclick="cancelarCalculo()">Cancelar</button>';
        }
    }
    
    private function getMensualidadesXMes($mes) {        
        $this->load->model("mcobranza");
        $mensualidades = $this->mcobranza->getFacturasMes($mes);
        if ($mensualidades) {
            $this->load->library('table');
            $this->table->set_template($this->tmpl);
            if (!$this->session->userdata('imprimir')) {
                $this->table->set_heading("id Factura", "Cliente", "Importe", "Estado");
                foreach ($mensualidades as $msldad){
                     $colEstado = array(
                        'data' => ($msldad->estado == "1" ? 'Pendiente':
                                  ($msldad->estado == "2" ? 'Pagado':'Suspendido')),
                        'class' =>($msldad->estado == "1" ? 'cel pendiente':
                                  ($msldad->estado == "2" ? 'cel pagado':'cel suspendido')));
                    $row_data = array($msldad->idfactura, $msldad->nombres." ".$msldad->apellidos,
                    "$ ".$msldad->importe_total,$colEstado); 
                    $this->table->add_row($row_data);
                }
            } else {
                $this->table->set_heading("Factura", "Cliente", "Importe", "Estado","Corte","Suspensión");
                foreach ($mensualidades as $msldad){
                    $colEstado = array(
                        'data' => ($msldad->estado == "1" ? 'Pendiente':
                                  ($msldad->estado == "2" ? 'Pagado':'Suspendido')),
                        'class' =>($msldad->estado == "1" ? 'cel pendiente':
                                  ($msldad->estado == "2" ? 'cel pagado':'cel suspendido')));
                    $row_data = array($msldad->idfactura, $msldad->nombres." ".$msldad->apellidos,
                        "$ ".$msldad->importe_total,$colEstado,$msldad->diacorte,$msldad->diaslimite); 
                    $this->table->add_row($row_data);
                }
            }
            $listado = $this->table->generate();
        } else {
            $listado = 'Sin Mensualidades';
        }
        return $listado;
    }        
    

//========================================================
//Setters
//--------------------------------------------------------
    private function setMensualidades($op = 'facturacion') {
        $this->session->set_userdata('operacion', $op);
        $mesfacturado=$this->session->userdata('mesfacturado');
        $data["operacion"] = $this->session->userdata('operacion');
        $this->load->model('mcobranza');
        $facturas=$this->mcobranza->getCalculoCortoMensualidades();
        $datosFacturas=array();
        foreach ($facturas as $factura){
            array_push($datosFacturas,array("importe_total"=>$factura['monto'],"mes"=>$mesfacturado,"anio"=>date('Y'),"estado"=>'1',"cliente_idcliente"=>$factura['cliente']));
        }
        $ids=count($datosFacturas); 
        foreach($datosFacturas as $datos){//crear una mensualidad por cliente //insertar las nuevas facturas del mes
            $this->mcobranza->setFacturacionMensualidades($datos);
        }
        $data["cuantos"]=$ids;
        $this->load->view('vdialogmensualidad', $data);
        $this->session->set_userdata('operacion', 'no');
    }
    
    private function setRegistroPago($idfactura) {
        $this->load->model('mcobranza');    
        $datosPago=array("usuario_id"=>$this->session->userdata('idusuario'),"mensualidad_idfactura"=>$idfactura,"fecha"=>$this->input->post('fecha'));
        $registrado=$this->mcobranza->setRegistroPago($datosPago);
        return $registrado;
    }
    
    function cancelarCalculoMensualidad(){
        $this->load->view('vDialogMensualidad');
    }
    
    private function imprimirFacturacion($mes){
        //$this->session->set_userdata('');
        
        $this->load->library('calendario');
        foreach($this->calendario->meses as $nm => $mm){
            if($mm==$mes){
                $mes=$nm;
            }
        }        
        $this->load->library('pdf');
        $stylesheet1= file_get_contents('css/layout.css');
        $this->pdf->WriteHTML($stylesheet1,1);
        $stylesheet3= file_get_contents('css/section_pdf.css');
        $this->pdf->WriteHTML($stylesheet3,1);
        $this->pdf->SetHeader('Facturación del mes de '.$this->calendario->mesesLargo[$mes].' | | Pág. {PAGENO}');
        $this->pdf->WriteHTML('<h2>Lista de Clientes con facturación</h2><p>1ra quincena del mes</p>'.$this->getMensualidadesXMes($mes),2);
        $this->pdf->SetFooter('Generado por: '.$this->session->userdata('usuario'));
        $this->session->set_userdata('imprimir',false);
        
        $this->pdf->Output();
    }
//---------------------------------------------------    
//Metodos complementarios
//---------------------------------------------------
   function getOperacion($operacionCobranza,$dato){
      $this->session->set_userdata('operCbza',$operacionCobranza);
      switch($operacionCobranza){
          case "aceptar":case "getMensualidad":case "registrarPago":    
              $this->session->set_userdata('idMens',$dato);
              $this->getPanelOperaciones();
          break;
          case "getCalculoMensualidades":  
              $this->session->set_userdata('idMes',$dato);
              $this->getCalculoMensualidad($dato);              
          break;
          case "getXMes":
                echo $this->getMensualidadesXMes($dato);
              break;
           case "generarMensualidad":
                $this->setMensualidades('facturacion');
              break;          
          case "validarDatosCliente":
              $this->validarDatosCliente();
          break;
      }
      
      
      
    }
    function getContenidoMain($contenido,$dato=1){
          switch ($contenido){
              case "getPagina":
                $this->session->set_userdata('mesIni',($this->session->userdata('mesIni')+$dato));               
                $this->getPanelMain();
                break;
              case "getImpresion":
                  $this->session->set_userdata('imprimir',true);
                  $this->imprimirFacturacion($dato);
                  
          }
      }
}

