<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * Operaciones CRUD para los registros de contratos
 */
class GestorDeContratos extends CI_Controller {

    var $tmpl = array(
                'table_open' => '<table class="scroll">',
                'heading_row_start' => '<tr class="basico">',
                'heading_row_end' => '</tr>',
                'row_start' => '<tr onclick="getContrato(this)" >',
                'row_end' => '</tr>',
                'row_alt_start' => '<tr onclick="getContrato(this)" >',
                'row_alt_end' => '</tr>'
            );
    
    function index() {
        if ($this->session->userdata('autenticado')) {
            $this->cargarPagina();
        } else {     //Si no hay sesion redirigir al login
            redirect('acceso', 'refresh');
        }
    }

    private function cargarPagina() {
        $session_data['usuario'] = $this->session->userdata('usuario');
        $data['usuario'] = $session_data['usuario'];
        $this->load->view('vheader', $data);

        //Cargar secciones de la interfaz
        $data["nav"] = $this->generarNavigation();
        $this->load->view('vnav', $data);
        $data["contenidoMain"] = $this->generarContenidoMain();
        $data["tabAct"] = 0;
        $this->load->view('vsectioncontratos', $data);
        $data = $this->generarSectionAside();
        $this->load->view('vasidecontratos', $data);

        $this->load->view('vfooter');
    }

    private function generarNavigation() {
        $active = array("act" => 3);
        $this->load->library("menu", $active);
        $this->menu->setActivo(3);
        return $this->menu->getMenu();
    }

    private function generarContenidoMain() {
        $servs = $this->getServicios();
        $this->load->library('tab');
        foreach ($servs as $filaServicio) {
            $this->tab->addTab($filaServicio->nombre, $this->listarContratos($filaServicio->idconcepto)); //$servs=  $this->getServicios();
        }
        return $this->tab->generar();
    }

    private function generarSectionAside() {
        $oper = $this->session->userdata('operacion'); //Verificar operacion para el panel aside
        switch ($oper) {
            case "getContratosXServicio":
                $id = $this->session->userdata('idserv');
                return $this->getContratosXServicio($id);
            //break;
            case "getContratosId":
                $id = $this->session->userdata('idserv');
                return $this->getContratoId($id);
            //break;
            default:
                $this->session->set_userdata('operacion', 'no');
                $asi["operacion"] = $oper;
                return $asi;
        }
    }
  /*
  * Create
  */
    function agregarContrato($op = 'nuevo') {
        $this->session->set_userdata('operacion', $op);
        $data["operacion"] = $this->session->userdata('operacion');
        $this->load->view('vasidecontratos', $data);
    }

    function validarDatosContrato() { //Solicita la validación des el Aside ed contratos
        if ($this->session->userdata('autenticado')) {
            $this->load->library('form_validation'); //This method will have the credentials validation
            $this->form_validation->set_rules('idcliente', 'Cliente', 'trim|required');
            $this->form_validation->set_rules('nombres', 'Nombre Cliente', 'trim|required');
            $this->form_validation->set_rules('servicio', 'Servicio Contratado', 'trim|required');
            $this->form_validation->set_rules('inicio', 'Fecha inicio', 'required');
            if ($this->form_validation->run() == FALSE) {//validacion fallida.  Solicitar revisión de datos
                echo '<div class="alerta">Verifique la informaci&oacute;n</div>';
                $this->agregarContrato('nuevo');
            } else { //Go to do operation         
                $this->guardarContrato();
            }
        }
    }

    private function guardarContrato(){ // Sirve para guardar la información indicada en el aside contrato despues de validar
        //Validación de campos exitosa.
        $this->load->model('MContrato', '', TRUE);
        $contrato["cliente_idcliente"] = $this->input->post('idcliente');
        $contrato["concepto_idconcepto"]=$this->input->post('servicio');
        $contrato["fechainicio"] = $this->input->post('inicio');
        $contrato["diacorte"] = $this->input->post('corte');
        $contrato["diaslimite"] = $this->input->post('limite');
        $idCnto=$this->input->post('idcontrato');
        if($idCnto!=0){
            $result = $this->MContrato->actualizarContrato($contrato,$idCnto);//query the database
            $msg='<div class="basico">Contrato Actualizado Exitosamente!!!</div>';
        }else{
            $result = $this->MContrato->agregarContrato($contrato);
            $msg='<div class="basico">Contrato Guardado Exitosamente!!!</div>';//query the database
        }
        if($result<1){
            $this->form_validation->set_message('guardarContrato', 'Los datos no se insertaron');
            $msg='<div class="basico">Contrato NO Asignado!!!</div>';
        }
        $this->session->set_userdata('operacion','no');
         echo $msg;
        } 
 /*
  * Read
  */
    private function getServicios() {
        $this->load->model('MServicio');
        $servicios = $this->MServicio->getServicios();
        return $servicios;
    }

    function getContratos($tabactiva=0) { //Despues de actualizar o guardar un contrato actualiza la tabla de la section principal
         if ($this->session->userdata('autenticado')) {
        $this->load->library('tab');
        $servs = $this->getServicios();
        $data["tabAct"] = $tabactiva;//$this->input->get('tabactiva');
        foreach ($servs as $filaServicio) {
            $this->tab->addTab($filaServicio->nombre, $this->listarContratos($filaServicio->idconcepto)); //$servs=  $this->getServicios();
        }
        $data["contenidoMain"] = $this->tab->generar();
        $this->load->view('vsectioncontratos', $data);
         }
    }

    private function listarContratos($idServicioCont) {
        $listado = '';
        $this->load->model("MContrato");
        $contratos = $this->MContrato->getContratosXServicio($idServicioCont);
        if ($contratos) {
            $this->load->library('table');
            $this->table->set_template($this->tmpl);
            $listado = $this->table->generate($contratos);
        } else {
            $listado = 'Sin Contratos';
        }
        return $listado;
    }

    function getContrato($id) {   // Sirve para generar el aside de información del contrato
         if ($this->session->userdata('autenticado')) {
            $this->load->model('MContrato');
            $contrato = $this->MContrato->getContratoId($id);
            if ($contrato) {
                $this->session->set_userdata('operacion', 'getContrato');
                $this->session->set_userdata('idcto', $id);
                $data["operacion"] = $this->session->userdata('operacion');
                $data["cto"] = $contrato[0];
                $this->load->view('vasidecontratos', $data);
            }
         }
    }

    function cancelarOperacion($op = 'no') {
        $this->session->set_userdata('operacion', $op);
        $data["operacion"] = $this->session->userdata('operacion');
        $this->load->view('vasidecontratos', $data);
    }
}