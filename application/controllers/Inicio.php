<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//session_start(); //we need to call PHP's session object to access it through CI
class Inicio extends CI_Controller {
 
 function __construct(){
   parent::__construct();
 }
 
 function index(){
 if($this->session->userdata('autenticado')){
     $session_data['usuario'] = $this->session->userdata('usuario');
     $data['usuario'] = $session_data['usuario'];
     $this->load->view('vheader', $data);
     $active=array("act"=>1);
     $this->load->library("menu",$active);
     $this->menu->setActivo(1);
     $data["nav"]=$this->menu->getMenu();
     $this->load->view('vnav',$data);
     $this->load->view('vsectioninicio');
     $this->load->view('vfooter');      
   }else{     //Si no hay sesion redirigir al login
      redirect('acceso', 'refresh');
   }
 }
         
 function logout(){
   //$this->session->unset_userdata('au');
   session_destroy();
   redirect('acceso', 'refresh');
 } 
}
