<!DOCTYPE html>
<html>
    <head>
        <title>Gesti&oacute;n de Clientes</title>
        <link rel="stylesheet" href="css/nav.css"/>
        <link rel="stylesheet" href="css/section.css"/>
        <link rel="stylesheet" href="css/aside.css"/>
        <link rel="stylesheet" href="css/layout.css"/>
        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/controlesdeinterfaz.js"></script>
        <script type="text/javascript" src="js/gestorclientes.js"></script>
        <script type="text/javascript" src="js/gestorcontratos.js"></script>
        <script type="text/javascript" src="js/gestorcobranza.js"></script>
    </head>
    <body>
        <div class="contenedor">
            <header class="encabezado"> 
                <h1>Sistema de clientes</h1>                
            </header>
            
        
