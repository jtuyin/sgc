  
<section class="panel principal" role="main">
    <!-- Contenedor central de información-->
    <?php echo $contenidoMain; ?>
<script>
var tabact=document.getElementsByClassName("tab");
<?php  
 echo 'cambiarTab("tab'.$tabAct.'",tabact['.$tabAct.'])';
?>
</script>
     <button class="button" onclick="mostrarDialog('generadorMensualidad')">Generar Mensualidad</button>
     <button class="button" onclick="imprimirFacturacion('mes')">Imprimir</button>
     <div class="dialog" id="generadorMensualidad">
         Seleccione el mes a generar
         <select name="mesSeleccionado">
            <?php
            $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
            'agosto','septiembre','octubre','noviembre','diciembre');
            for ($mes = 0; $mes < 12; $mes++) {
                if ($mes == date("n")) {
                echo '<option selected value="' . ($mes + 1) . '">' . $meses[$mes] . '</option>';
                } else{
                echo '<option value="' . ($mes + 1) . '">' . $meses[$mes] . '</option>';
                }
            }
?>               
         </select>
         <button id="generadorMensualidadBtn" class="button buttonNaranja" onclick="calcularMensualidad()">Generar Mensualidad</button>
         <button id="generadorMensualidadBtnCancelar" class="button" onclick="cancelarCalculo()">Cancelar</button>
     </div>
</section>

