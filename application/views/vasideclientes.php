<aside class="panel operaciones">
<!-- Contenedor de operaciones del gestor Clientes--> 
<?php
    switch ($operacion){
            case "getCliente":{
            ?>
                
  <div class="basico">Informaci&oacute;n del Cliente</div> 
                <form id="frmctes" action="gestordeclientes">
                        <label for="fnombres">Nombres</label>
                        <input type="text" id="fnombres" name="nombres" value="<?php echo $cte->nombres;?>">

                        <label for="fapellidos">Apellidos</label>
                        <input type="text" id="fapellidos" name="apellidos" value="<?php echo $cte->apellidos;?>">
                        
                        <label for="fdireccion">Direcci&oacute;n</label>
                        <input type="text" id="fdireccion" name="direccion" value="<?php echo $cte->direccion;?>">

                        <label for="fcorreo">Correo</label>
                        <input type="text" id="fcorreo" name="correo" value="<?php echo $cte->correo; ?>">

                        <label for="ftelefono">Tel&eacute;fono</label>
                        <input type="text" id="ftelefono" name="telefono" value="<?php echo $cte->telefono; ?>">
                        <input type="hidden" id="fid" name="idcliente" value="<?php echo $cte->idcliente; ?>">

                        <label for="flocalidad">Localidad</label>
                        <select id="flocalidad" name="localidad">
                            
                            <option value="1" <?php if ($cte->localidad_idlocalidad==1) echo 'selected'; ?>>San Ignacio</option>
                            <option value="2" <?php if ($cte->localidad_idlocalidad==2) echo 'selected'; ?>>Dzidzilch&eacute;</option>
                            <option value="3" <?php if ($cte->localidad_idlocalidad==3) echo 'selected'; ?>>Para&iacute;so</option>
                        </select>    
                        
                </form>
                <button class="button" onclick="cancelarOperacionCliente()">Aceptar</button> 
                <button class="button buttonNaranja" onclick="guardarCliente($('#frmctes'))">Guardar Cambios</button> 
            <?php
            break;}
            
            case "setCliente":{
?>         
            <div class="basico">Agregar Cliente</div> 
                <form id="frmctes" action="gestordeclientes">
                        <label for="fnombres">Nombres</label>
                        <input type="text" id="fnombres" name="nombres" autocomplete="off">

                        <label for="fapellidos">Apellidos</label>
                        <input type="text" id="fapellidos" name="apellidos" autocomplete="off">
                        
                        <label for="fdireccion">Direcci&oacute;n</label>
                        <input type="text" id="fdireccion" name="direccion">

                        <label for="fcorreo">Correo</label>
                        <input type="text" id="fcorreo" name="correo" autocomplete="off">

                        <label for="ftelefono">Tel&eacute;fono</label>
                        <input type="text" id="ftelefono" name="telefono" autocomplete="off">
                        <input type="hidden" id="fid" name="idcliente" value="0">    
                        <label for="flocalidad">Localidad</label>
                        <select id="flocalidad" name="localidad">
                            <option value="1">San Ignacio</option>
                            <option value="2">Dzidzilch&eacute;</option>
                            <option value="3">Para&iacute;so</option>
                        </select>    
                        
                </form>
                <button class="button" onclick="guardarCliente($('#frmctes'))">Guardar</button> 
                <button class="button buttonNaranja" onclick="cancelarOperacionCliente()">Cancelar</button> 
<?php
            break;}
            //case "guardarCliente":
            default:
                echo $mensaje;
        }
    ?>
</aside>
      
