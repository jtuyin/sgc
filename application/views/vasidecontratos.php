  <!-- Contenedor de operaciones del gestor Contratos--> 
<aside class="panel operaciones">
<?php    
 
        switch ($operacion){
            case "no":{
                    echo '<div class="basico basico-borde">De clic en un servicio contratado </div>';
            }break;
            case "getContrato":{
?>   
            <div class="basico">Informaci&oacute;n del Servicio Contratado</div> 
                <form id="frmcontratos" action="gestordecontratos/actualizarContrato">
                       <label for="fnombres">Nombres</label>
                       <input type="text" id="fnombres" name="nombres" autocomplete="off" value="<?php echo $cto->Nombre;?>" >
                       <div id="resultados"> </div>
                       <label for="fservicio">Servicio</label>
                        <select id="fServicio" name="servicio">
                            <option value="1" <?php if ($cto->servicio_idservicio==1) echo 'selected'; ?>>3Mbps</option>
                            <option value="2" <?php if ($cto->servicio_idservicio==2) echo 'selected'; ?>>4Mbps</option>
                            <option value="3" <?php if ($cto->servicio_idservicio==3) echo 'selected'; ?>>5Mbps</option>
                        </select>
                       <label for="ffecha">Fecha de inicio</label><br>
                        <input type="date" id="ffecha" name="inicio" value="<?php echo $cto->fechainicio;?>"><br>
                        <label for="fcorte">D&iacute;a corte del mes</label><br>
                        <input type="number" id="fcorte" name="corte" min="1" max="31" value="<?php echo $cto->diacorte;?>"><br>
                        <label for="flimite">D&iacute;as  para L&iacute;mitar</label><br>
                        <input type="number" id="flimite" name="limite" min="0" max="15" value="<?php echo $cto->diaslimite;?>"><br>
                        <input type="hidden" id="fid" name="idcliente" value="<?php echo $cto->cliente_idcliente;?>">
                        <input type="hidden" id="fid" name="idcontrato" value="<?php echo $cto->idcontrato;?>">
                </form>
  
                <button class="button" onclick="cancelarOperacionContrato()">Aceptar</button> 
                <button class="button buttonNaranja" onclick="guardarContrato($('#frmcontratos'))">Guardar Cambios</button> 
<?php
            }break;            
            case "nuevo":{
?>         
            <div class="basico">Agregando un nuevo contrato</div> 
                <form id="frmcontratos" action="gestordecontratos/agregarContrato">
                       <label for="fnombres">Nombres</label>
                       <input type="text" id="fnombres" name="nombres" autocomplete="off" onkeyup="buscarCliente(this.value)">
                       <div id="resultados"> </div>
                       <label for="fservicio">Servicio</label>
                        <select id="fServicio" name="servicio">
                            <option value="1">3Mbps</option>
                            <option value="2">4Mbps</option>
                            <option value="3">5Mbps</option>
                        </select>
                       <label for="ffecha">Fecha de inicio</label><br>
                        <input type="date" id="ffecha" name="inicio"><br>
                        <label for="fcorte">D&iacute;a corte del mes</label><br>
                        <input type="number" id="fcorte" name="corte" min="1" max="31" value="7"><br>
                        <label for="flimite">D&iacute;as  para L&iacute;mitar</label><br>
                        <input type="number" id="flimite" name="limite" min="0" max="15" value="7"><br>
                        <input type="hidden" id="fid" name="idcliente" value="0">
                        <input type="hidden" id="fid" name="idcontrato" value="0">
                </form>
                <button class="button" onclick="guardarContrato($('#frmcontratos'))">Contratar</button> 
                <button class="button buttonNaranja" onclick="cancelarOperacionContrato()">Cancelar</button> 
                
<?php
            }break;
            case "actualizar":{
?>                
            <div class="basico">Informaci&oacute;n del Servicio Contratado</div> 
                <form id="frmctes" action="gestordecontratos/agregarCliente">
                        <label for="fnombres">Nombres</label>
                        <input type="text" id="fnombres" name="nombres" value="<?php echo $cte->nombres;?>">

                        <label for="fapellidos">Apellidos</label>
                        <input type="text" id="fapellidos" name="apellidos" value="<?php echo $cte->apellidos;?>">
                        
                        <label for="fdireccion">Fecha de Inicio</label>
                        <input type="text" id="fdireccion" name="direccion" value="<?php echo $cte->direccion;?>">

                        <label for="fcorreo">Corte</label>
                        <input type="text" id="fcorreo" name="correo" value="<?php echo $cte->correo; ?>">

                        <label for="ftelefono">Tel&eacute;fono</label>
                        <input type="text" id="ftelefono" name="telefono" value="<?php echo $cte->telefono; ?>">
                        <input type="hidden" id="fid" name="idcliente" value="<?php echo $cte->idcliente; ?>">

                        <label for="flocalidad">Servicio</label>
                        <select id="flocalidad" name="localidad">                            
                            <option value="1" <?php if ($cte->localidad_idlocalidad==1) echo 'selected'; ?>>San Ignacio</option>
                            <option value="2" <?php if ($cte->localidad_idlocalidad==2) echo 'selected'; ?>>Dzidzilch&eacute;</option>
                            <option value="3" <?php if ($cte->localidad_idlocalidad==3) echo 'selected'; ?>>Para&iacute;so</option>
                        </select>                            
                </form>
                <button class="button" onclick="cancelarOperacionContrato()">Aceptar</button> 
                <button class="button buttonNaranja" onclick="guardarContrato($('#frmctes'))">Guardar Cambios</button> 
            <?php
            }break;    
        }
    ?>
</aside>
      
