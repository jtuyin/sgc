<div class="dialog" id="mapaInstalaciones">
    <?php if(isset($cuantos)){
     echo '<p>'.$cuantos.' registros insertados correctamente </p>';
     echo ' <button id="generadorMensualidadBtnCancelar" class="button" onclick="finalizarFacturacion()">Aceptar</button>';
    }  else {
    ?>

         Seleccione el mes a generar
         <select name="mesSeleccionado">
            <?php
                $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
                'agosto','septiembre','octubre','noviembre','diciembre');
                for ($mes=0;$mes<12;$mes++) {
                    if ($mes == date("n")) {
                echo '<option selected value="' . ($mes) . '">' . $meses[$mes] . '</option>';
                } else{
                echo '<option value="' . ($mes) . '">' . $meses[$mes] . '</option>';
                }
               }
            ?>               
         </select>
         <button id="generadorMensualidadBtn" class="button buttonNaranja" onclick="calcularMensualidad()">Generar Mensualidad</button>
         <button id="generadorMensualidadBtnCancelar" class="button" onclick="cancelarCalculo()">Cancelar</button>
<?php
}
?>
</div>