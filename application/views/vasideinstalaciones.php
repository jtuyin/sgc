<aside class="panel operaciones">
<!-- Contenedor de operaciones del gestor Instalaciones--> 
<?php
    switch ($operacion){
            case "getInstalacion":{
            ?>
                
  <div class="basico">Informaci&oacute;n del Instalacion</div> 
                <form id="frminsts" action="gestordeinstalaciones">
                        <label for="fnombres">Nombres</label>
                        <input type="text" id="fnombres" name="nombres" value="<?php echo $inst->nombres;?>">

                        <label for="fapellidos">Apellidos</label>
                        <input type="text" id="fapellidos" name="apellidos" value="<?php echo $inst->apellidos;?>">
                        
                        <label for="fdireccion">Direcci&oacute;n</label>
                        <input type="text" id="fdireccion" name="direccion" value="<?php echo $inst->direccion;?>">

                        <label for="fcorreo">Correo</label>
                        <input type="text" id="fcorreo" name="correo" value="<?php echo $inst->correo; ?>">

                        <label for="ftelefono">Tel&eacute;fono</label>
                        <input type="text" id="ftelefono" name="telefono" value="<?php echo $inst->telefono; ?>">
                        <input type="hidden" id="fid" name="idcliente" value="<?php echo $inst->idcliente; ?>">

                        <label for="flocalidad">Localidad</label>
                        <select id="flocalidad" name="localidad">
                            
                            <option value="1" <?php if ($inst->localidad_idlocalidad==1) echo 'seleinstd'; ?>>San Ignacio</option>
                            <option value="2" <?php if ($inst->localidad_idlocalidad==2) echo 'seleinstd'; ?>>Dzidzilch&eacute;</option>
                            <option value="3" <?php if ($inst->localidad_idlocalidad==3) echo 'seleinstd'; ?>>Para&iacute;so</option>
                        </select>    
                        
                </form>
                <button class="button" onclick="cancelarOperacionInstalacion()">Aceptar</button> 
                <button class="button buttonNaranja" onclick="guardarInstalacion($('#frminsts'))">Guardar Cambios</button> 
            <?php
            break;}
            
            case "setInstalacion":{
?>         
                <div class="basico">Agregar informaci&oacute;n de la instalacion</div> 
                <form id="frminsts" action="gestordeinstalaciones">
                        <label for="fcliente">Equipo</label>
                        <input type="text" id="fcliente" name="cliente" autocomplete="off">
                        <label for="fequipo">Equipo</label>
                        <input type="text" id="fequipo" name="equipo" autocomplete="off">

                        <label for="fip">Direcci&oacute;n IP</label>
                        <input type="text" id="fip" name="ip" autocomplete="off">
                        
                        <label for="fubicacion">Ubicacion</label>
                        <input type="text" id="fubicacion" name="ubicacion">
                        <label for="fap">Punto de Acceso</label>
                        <input type="text" id="fap" name="AP" autocomplete="off">
                        <label for="fobservacion">Observacion</label>
                        <input type="text" id="fobservacion" name="observacion" autocomplete="off">

                        
                        <input type="hidden" id="fid" name="idcliente" value="0">    
                        
                        
                </form>
                <button class="button" onclick="guardarInstalacion($('#frminsts'))">Guardar</button> 
                <button class="button buttonNaranja" onclick="cancelarOperacionInstalacion()">Cancelar</button> 
<?php
            break;}
            //case "guardarInstalacion":
            default:
                echo $mensaje;
        }
    ?>
</aside>
      
