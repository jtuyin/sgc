 <!-- Contenedor de operaciones del gestor Contratos--> 
<aside class="panel operaciones">
   
<?php   switch ($operacion){
            case "getMensualidad":
?>   
                <div class="basico">Informaci&oacute;n de la Factura</div> 
                    <form id="frmcobranza" action="gestordecobranza">
                        <label for="fnombres">Nombre</label>
                        <input type="text" id="fnombres" name="nombres" disabled value="<?php echo $mensualidad->nombres . " " . $mensualidad->apellidos; ?>" >
                        <label for="fcorreo">Correo</label>
                        <input type="text" id="fcorreo" name="correo" disabled value="<?php echo $mensualidad->correo; ?>" >
                        <label for="fmes">Mes</label>
                        <input type="text" id="fmes" name="mesnombre" disabled value="<?php echo $mensualidad->mesNombre; ?>" >
                        <label for="fimporte">Importe</label>
                        <input type="text" id="fimporte" name="importe" disabled value="<?php echo $mensualidad->importe_total; echo $mensualidad->estado == "2" ? ' (Pagado)' : ''?>" >
                        <label for="ffecha">Fecha</label>
                        <input type="date" name="ffecha" step="1" <?php echo 'value="'.date("d-m-Y").'"';echo $mensualidad->estado == "2" ? 'disabled' : ''?> >
                        <input type="hidden" id="fidmes" name="idmes" value="<?php echo $mensualidad->mes; ?>">
                        <input type="hidden" id="fidmensualidad" name="idmensualidad" value="<?php echo $mensualidad->idfactura; ?>">
                        <input type="hidden" id="fidcliente" name="idcliente" value="<?php echo $mensualidad->idcliente; ?>">
                    </form>
                    <button class="button" onclick="cancelarOperacionCobranza()">Aceptar</button> 
                    <button  <?php echo $mensualidad->estado == "2" ? 'class="button buttonGris" disabled' : 'class="button buttonNaranja"'?> onclick="registrarPago($('#frmcobranza'))">Registrar Pago</button> 
<?php       break;            
            case "setPago":{ echo $pago;
?>         
                <div class="basico">El pago se ha registrado</div> 
                    <form id="frmnota" action="gestordecontratos/agregarContrato">
                       <label for="fnombres">Nombres</label>
                       <input type="text" id="fnombres" name="nombres" autocomplete="off" onkeyup="buscarCliente(this.value)">
                       <div id="resultados"> </div>
                       <label for="fservicio">Servicio</label>
                        <select id="fServicio" name="servicio">
                            <option value="1">3Mbps</option>
                            <option value="2">4Mbps</option>
                            <option value="3">5Mbps</option>
                        </select>
                       <label for="ffecha">Fecha de inicio</label><br>
                        <input type="date" id="ffecha" name="inicio"><br>
                        <label for="fcorte">D&iacute;a corte del mes</label><br>
                        <input type="number" id="fcorte" name="corte" min="1" max="31" value="7"><br>
                        <label for="flimite">D&iacute;as  para L&iacute;mitar</label><br>
                        <input type="number" id="flimite" name="limite" min="0" max="15" value="7"><br>
                        <input type="hidden" id="fid" name="idcliente" value="0">
                        <input type="hidden" id="fid" name="idcontrato" value="0">
                </form>
                <button class="button" onclick="guardarContrato($('#frmnota'))">Imprimir Nota</button> 
                <button class="button buttonNaranja" onclick="cancelarOperacionImpresion()">Cancelar</button> 
                
<?php
            }break;
            case "agregar":{ //Agregar Concepto?>                
                <div class="basico">Informaci&oacute;n de la Facturaci&oacute;n</div> 
                <form id="frmnota" action="gestordecobranza">                   
                        <label for="fnombres">Nombres</label>
                        <input type="text" id="fnombres" name="nombres" value="<?php echo $cte->nombres;?>">

                        <label for="fapellidos">Apellidos</label>
                        <input type="text" id="fapellidos" name="apellidos" value="<?php echo $cte->apellidos;?>">
                        
                        <label for="fdireccion">Fecha de Inicio</label>
                        <input type="text" id="fdireccion" name="direccion" value="<?php echo $cte->direccion;?>">

                        <label for="fcorreo">Corte</label>
                        <input type="text" id="fcorreo" name="correo" value="<?php echo $cte->correo; ?>">

                        <label for="ftelefono">Tel&eacute;fono</label>
                        <input type="text" id="ftelefono" name="telefono" value="<?php echo $cte->telefono; ?>">
                        <input type="hidden" id="fidcliente" name="idcliente" value="<?php echo $cte->idcliente; ?>">

                        <label for="flocalidad">Servicio</label>
                        <select id="flocalidad" name="localidad">                            
                            <option value="1" <?php if ($cte->localidad_idlocalidad==1) echo 'selected'; ?>>San Ignacio</option>
                            <option value="2" <?php if ($cte->localidad_idlocalidad==2) echo 'selected'; ?>>Dzidzilch&eacute;</option>
                            <option value="3" <?php if ($cte->localidad_idlocalidad==3) echo 'selected'; ?>>Para&iacute;so</option>
                        </select>                            
                </form>
                <button class="button" onclick="cancelarOperacionContrato()">Aceptar</button> 
                <button class="button buttonNaranja" onclick="guardarContrato($('#frmctes'))">Guardar Cambios</button> 
            <?php
            }break;
        default :
            echo $mensaje;
        }
?>
</aside>
      
