<?php
/**
 * Description of NavTab
 * @author jtuyin
 */
class Tab {
    //put your code here
    var $tabTitles=array();
    var $tabContents=array();
    var $controlTab='';
    var $tabsSelector=FALSE;
    var $tabsPaginador=FALSE;
    var $urlRegresar;
    var $urlAvanzar;
    var $selector;
    var $paginador;
        
    public function addTab($titulo,$contenido){
        array_push($this->tabTitles,$titulo);
        array_push($this->tabContents,$contenido);     
    }
    
    public function addPaginador($ref){
        $this->tabsPaginador=TRUE;
        $this->paginador=$ref;
    }
     public function addSelector($ref){
        $this->tabsSelector=TRUE;
        $this->selector=$ref;
    }
    
    private function setPestanias() {//pestañas del tabstrip
        $this->controlTab = '<ul class="navtab">';
        foreach ($this->tabTitles as $idTab => $tituloTab):
            $this->controlTab = $this->controlTab . '<li><a href="#" class="tab" onclick="cambiarTab(\'tab'.$idTab.'\',this)">' . $tituloTab . '</a></li>';
        endforeach;
        if($this->tabsPaginador){
          $this->setPaginador();
        }
        if($this->tabsSelector){
          $this->setSelector();
        }
         
        $this->controlTab = $this->controlTab . '</ul>';
    }
    private function setSelector(){
         $this->controlTab =$this->controlTab.'<div><select name="selectanio" style="padding: 7px 20px;"><option value="2017">2017</option><option value="2016">2016</option></select></div>';
     }
     private function setPaginador(){
         $this->controlTab =$this->controlTab. '<div>';
        
         if(($this->paginador-3)>0){
            $this->controlTab = $this->controlTab . '<button class="buttontabpaginador" onclick="cambiarPaginaTabs('.($this->paginador-3).',this)"><</button>';   
         }else{
            $this->controlTab = $this->controlTab . '<button class="buttontabpaginador-disabled" disabled><</button>';   
         }
         if(($this->paginador+2)<13){
            $this->controlTab = $this->controlTab . '<button class="buttontabpaginador" onclick="cambiarPaginaTabs('.($this->paginador+2).',this)">></button>';
         }else{
            $this->controlTab = $this->controlTab . '<button class="buttontabpaginador-disabled">></button>'; 
         }
          
        
        $this->controlTab = $this->controlTab . '</div>';
    }

    function setContenidos() {
        foreach ($this->tabContents as $idCont => $contenidoTab):
            $this->controlTab=$this->controlTab
            . '<div id="tab'. $idCont .'" class="tabpanel">'
            . $contenidoTab
            . '</div>';
        endforeach;
    }
    
    public function generar(){
        $this->setPestanias();
        $this->setContenidos();       
        return $this->controlTab;
    }
    
            
}
