<?php
/**
 * Description of Menu
 * Recibe El arreglo y indice activo 
 * y genera el contenido html del bloque nav 
 * @author jtuyin
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Calendario{
    var $meses = array(
        1=>"Ene",2=>"Feb",3=>"Mar",4=>"Abr",
        5=>"May",6=>"Jun",7=>"Jul",8=>"Ago",
        9=>"Sep",10=>"Oct",11=>"Nov",12=>"Dic");
    var $mesesLargo = array(
        1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",
        5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",
        9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
    var $inicio;
     
    function setMesActivo($ini){
        $this->inicio=$ini;
    }
            
    function getMeses() {
        for($i=$this->inicio-2;$i<($this->inicio+2);$i++){
            $meses[$i]=$this->meses[$i];
        }
        return $meses;
    }
    function getMes($mesSol) {
        return $this->mesesLargo[$mesSol];
    }
}