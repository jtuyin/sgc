<?php
/**
 * Description of Menu
 * Recibe El arreglo y indice activo 
 * y genera el contenido html del bloque nav 
 * @author jtuyin
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu{
    var $items =array(
        "Inicio"=>array(1,"inicio"),
        "Clientes"=>array(2,"GestorDeClientes"),
        "Contratos"=>array(3,"GestorDeContratos"),
        "Cobranza"=>array(4,"gestordecobranza"),
        "Instalaciones"=>array(5,"gestordeinstalaciones"),
        "Reportes"=>array(6,"#"),
        "Configurar"=>array(7,"#"),
        "Salir"=>array(8,'inicio/logout')
        );
    var $activo;
    
     public function __construct($act){
        $this->activo=$act;
    }
            
    function getMenu(){
        $nav='<ul class="menu">';
        foreach ($this->items as $key => $value) {
            if($this->activo==$value[0]){
                $nav=$nav.'<li ><a class="activo" href="#">'.$key.'</a></li>'; //role="presentation" class="active" '.base_url()."main/".$value[1].'
            }  
            else {
               $nav=$nav.'<li ><a class="basico" href="'.$value[1].'">'.$key.'</a></li>';
               //$nav=$nav.'<li ><a class="basico" href="'.base_url().$value[1].'">'.$key.'</a></li>';
            }
        }
        $nav=$nav.'</ul></nav>';
        return $nav;
    }    
    function setActivo($act) {
        $this->activo=$act;
    }
}