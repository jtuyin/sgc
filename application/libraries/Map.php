<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Maps Library
 *
 * Generate Maps in your CodeIgniter applications.
 *
 * @package	        CodeIgniter
 * @subpackage		Libraries
 * @category		Libraries
 * @author			Yudha Pratama
 * @license			None
 * @link			https://github.com/shinryu99/ci-mpdf
 */
require_once(dirname(__FILE__) . '/cimaps/Googlemaps.php');
class Map extends Googlemaps
{
	/**
	 * Get an instance of CodeIgniter
	 *
	 * @access	protected
	 * @return	void
	
	protected function ci()
	{
		return get_instance();
	} */
	/**
	 * Load a CodeIgniter view into domPDF
	 *
	 * @access	public
	 * @param	string	$view The view to load
	 * @param	array	$data The view data
	 * @return	void
	 
	public function load_view($view, $data = array())
	{
		$html = $this->ci()->load->view($view, $data, TRUE);
		$this->WriteHtml($html);
	}*/
    
    function load($param=NULL){
        
            if ($param == NULL){
		//$param = '"en-GB-x","Letter","","",10,10,10,10,6,3';
                $config['center'] = '37.4419, -122.1419';
                $config['zoom'] = 'auto';
            }
            return new Googlemaps($param);
    }
}